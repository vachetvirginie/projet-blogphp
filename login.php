<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="create.css">

    <title>Document</title>
</head>

<body>
    <header>
        <img class="logo" STYLE="float:top-left;" src="http://data.whicdn.com/images/109326916/original.png">
        <div class="menu" STYLE="float:right;">
            <ul>

                <li><a href="#">Work</a></li>

                <li><a href="#">About </a></li>

                <li> <a href="#">Contact </a></li>
            </ul>
        </div>
    </header>
    <h1>Login</h1>
    <main>
        <form action="index.php" method="POST">
            <div>
                <label>Nom d'utilisateur <br/></label>
                <input type="text" name="username" placeholder="Nom d'utilisateur">
            </div>

            <div>
                <label>Mot de Passe <br/></label>
                <input type="password" name="password" placeholder="Tapez votre mot de passe ici">
            </div>
            <button> Se connecter </button>



        </form>

    </main>
<?php 
    if(isset($_POST['pseudo']) 
        && isset($_POST['password'])){
            //On stock les infos du form en variable
        $pseudo = htmlspecialchars($_POST['pseudo']);
        $mdp = md5($_POST['password']);
        //un utilisateur existe si le fichier correspondant
        //existe (on teste avec is_file)
        if(is_file('utilisateur/'.$pseudo.'.txt')){
            //On récupère le contenu du fichier, le mdp crypté
            $contenu = file_get_contents('utilisateur/'.$pseudo.'.txt');
            //Si le contenu correspond au mdp entré par
            //l'utilisateur, la connexion est effective
            if($contenu == $mdp) {
                //On lance la session
                session_start();
                //On stock le nom de l'utilisateur dans celle ci
                $_SESSION['utilisateur'] = $pseudo;
                echo 'vous êtes bien connecté.e';
            }else{
                //sinon messages d'erreur
                echo 'l\'utilisateur ou le mdp n\'existe pas';
            }
            
        } else {
                //sinon messages d'erreur
            echo 'l\'utilisateur ou le mdp n\'existe pas';
        }
    }
    ?>
</body>

</html>